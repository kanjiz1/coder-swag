//
//  Category.swift
//  Coder-swag
//
//  Created by Oforkanji Odekpe on 1/13/18.
//  Copyright © 2018 Oforkanji Odekpe. All rights reserved.
//

import Foundation

struct Category{
    private (set) public var title: String
    private (set) public var imageName: String
    
    init(title: String, imageName: String){
        self.title = title
        self.imageName = imageName
    }
}
